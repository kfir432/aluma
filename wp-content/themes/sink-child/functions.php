<?php
	defined( 'ABSPATH' ) or die( 'Keep Silent' );

//-------------------------------------------------------------------------------
// Enqueue scripts and styles.
//-------------------------------------------------------------------------------

if ( ! function_exists( 'hippo_scripts' ) ) :

    function hippo_scripts() {

        $suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

        do_action( 'hippo_before_enqueue_scripts' );

        if ( ! hippo_option( 'body-typography', 'font-family' ) ) {
            wp_enqueue_style( 'google-font', hippo_fonts_url(), array(), NULL );
        }


        // Material-design-icons
        wp_enqueue_style( 'hippo-material-design-icons', get_template_directory_uri() . '/css/material-design-iconic-font.min.css', array(), '2.1.2' );

        // Font Awesome Icons
        wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.css', array(), '4.4.0' );

        // Animate css
        wp_enqueue_style( 'animate', get_template_directory_uri() . '/css/animate.css', array(), NULL );

        hippo_enqueue_twitter_bootstrap();

        if ( is_active_widget( FALSE, FALSE, 'hippo_latest_tweet', TRUE ) ) :

            hippo_enqueue_owl_carousel();
        endif;

        if ( ( function_exists( 'is_product' ) && ! is_product() ) ) {
            hippo_enqueue_magnific_popup();
        }

        // hippo-offcanvas
        wp_enqueue_style( 'hippo-offcanvas', get_template_directory_uri() . '/css/hippo-off-canvas.css', array(), NULL );


        if ( class_exists( 'Hippo_Less_Css_Init' ) ) {
            wp_enqueue_style( 'master-less', hippo_locate_template_uri( 'less/master.less' ) );
        }
        else {
            wp_enqueue_style( 'hippo-main-css', sprintf( '%s/css-compiled/master-%s.css', get_template_directory_uri(), hippo_option_get_preset() ) );
        }
        // main stylesheet
        wp_enqueue_style( 'stylesheet', get_stylesheet_uri() );

        if(ICL_LANGUAGE_CODE == 'he'){
            wp_register_style( 'bootstrap-rtl', get_template_directory_uri() . "/css/bootstrap-rtl.min.css", array(), '3.3.5' );
            wp_enqueue_style( 'bootstrap-rtl' );
            wp_register_style( 'custom-style', get_template_directory_uri() . "/css/sass/custom.css", array(), '3.3.5' );
            wp_enqueue_style( 'custom-style' );
            wp_register_style( 'rtl-style', get_template_directory_uri() . "/css/sass/rtl.css", array(), '3.3.5' );
            wp_enqueue_style( 'rtl-style' );
        }

        do_action( 'hippo_after_enqueue_styles' );

        // modernizr
        wp_enqueue_script( 'hippo-modernizr', get_template_directory_uri() . '/js/modernizr-2.8.1.min.js', array(), NULL );


        // Hippo offcanvas
        wp_enqueue_script( 'hippo-off-canvas', get_template_directory_uri() . '/js/hippo-off-canvas.js', array( 'jquery' ), NULL, TRUE );

        if ( hippo_option( 'sticky-menu', FALSE, TRUE ) ) {
            // Sticky menu js
            wp_enqueue_script( 'hippo-sticky-menu', get_template_directory_uri() . '/js/sticky-menu.js', array( 'jquery' ), NULL, TRUE );
        }

        if ( hippo_option( 'retina-ready', FALSE, FALSE ) ):
            // Retina js
            wp_enqueue_script( 'hippo-retina', get_template_directory_uri() . '/js/retina.min.js', array( 'jquery' ), NULL, TRUE );
        endif;

        if ( is_active_widget( FALSE, FALSE, 'hippo_flickr_photo', TRUE ) ) :
            hippo_enqueue_flickr_feed();
        endif;

        if ( substr( basename( get_page_template_slug() ), 0, 9 ) == 'blog-grid' ) :
            // Masonry
            wp_enqueue_script( 'jquery-masonry' );
        endif;

        // plugin
        wp_enqueue_script( 'hippo-script', get_template_directory_uri() . '/js/scripts.js', array( 'jquery', 'wp-util' ), NULL, TRUE );

        // localize script
        wp_localize_script( 'hippo-script', 'hippoJSObject', apply_filters( 'hippo_js_object', array(
            'ajax_url'                => esc_url( admin_url( 'admin-ajax.php' ) ),
            'site_url'                => esc_url( site_url( '/' ) ),
            'home_url'                => esc_url( home_url( '/' ) ),
            'theme_url'               => get_template_directory_uri(),
            'is_front_page'           => is_front_page(),
            'is_home'                 => is_home(),
            'offcanvas_menu_position' => 'hippo-offcanvas-' . hippo_option( 'offcanvas-menu-position', FALSE, 'left' ),
            'offcanvas_menu_effect'   => hippo_option( 'offcanvas-menu-effect', FALSE, 'reveal' ),
            'currency_switcher'       => hippo_option( 'currency-switcher', FALSE, FALSE ),
            'back_to_top'             => hippo_option( 'back-to-top', FALSE, TRUE )
        ) ) );


        if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) :
            wp_enqueue_script( 'comment-reply' );
        endif;

        do_action( 'hippo_after_enqueue_scripts' );
    }

    add_action( 'wp_enqueue_scripts', 'hippo_scripts', 11 );
endif;
